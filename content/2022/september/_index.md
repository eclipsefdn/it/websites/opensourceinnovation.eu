---
title: "Open Research Webinars"
event: "September 20, 2022 - 16:00-17:00 CET"
completed: true
date: 2022-09-12T16:00:00+02:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
hide_breadcrumb: true
container: container-fluid
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
links: [[href: "#speakers", text: Speakers], [href: "#agenda", text: Agenda]]
layout: single
---


{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
	<!--
	<h1 style="color: black; text-align: center;">The agenda and speakers for the next event will be announced soon!</h1>
-->
<!-- Comment this line when event is finished -->
{{< events/agenda event="september" year="2022" >}}
<!-- ******** -->
{{</ grid/section-container >}}


<!-- Comment this block when event is finished -->
{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="september" year="2022" title="Speakers" source="speakers" imageRoot="/2022/september/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}
<!-- ******** -->



{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg" >}}
   {{< events/registration event="september" year="2022" title="About Open Research Webinars" >}} 

Through a selection of state-of-the-art project presentations and demonstrations, this series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.

### This episode features presentations of:
[![SmartCLIDE](/2022/september/images/smartclide.png)](https://smartclide.eu) [![Spoon](/2022/september/images/spoon.png)](https://spoon.gforge.inria.fr/)

   {{</ events/registration >}}
{{</ grid/section-container >}}



{{< grid/section-container id="projects" class="featured-section-row featured-section-row-dark-bg">}} 
	{{< past_projects title="Presented Projects">}}
{{</ grid/section-container >}}

{{< grid/section-container id="events" class="featured-section-row featured-section-row-light-bg">}}
	{{< past_events title="Past Events">}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}