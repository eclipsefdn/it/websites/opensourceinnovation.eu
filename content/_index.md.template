---
title: "Open Research Webinars"
tagline: "Open collaboration in European Research Projects"
event: "Next Episode: November 7, 2023 - 16:00-17:00 CET (Speakers TBA)"
date: 2020-12-15T16:00:00+02:00
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
layout: single
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
container: container-fluid
links: [[href: "#projects", text: Watch previous episodes], [href: "http://eepurl.com/hrvCe9", text: Subscribe to newsletter]]
---


{{< grid/section-container id="about" class="featured-section-row featured-section-row-lighter-bg" >}}
{{< grid/section-container class="featured-section-row text-center">}}
<h1>About Open Research Webinars</h1>
<p>
Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.
</p>
<h2 id="next">Next episode: November 7, 2023 - 16:00-17:00 CET (Speakers TBA)</h2>
<!--
<a href="https://eclipse.org/steady" target="_blank"><img src="2022/january/images/steady.png" width="200" alt="Steady"></a>
<a href="https://www.sat4j.org/" target="_blank"><img src="2022/january/images/sat4j.png" width="200" alt="Sat4j"></a>
-->
{{</ grid/section-container >}}
{{</ grid/section-container >}}


{{< grid/section-container id="projects" class="featured-section-row featured-section-row-dark-bg">}} 
	{{< past_projects title="Presented Projects">}}
{{</ grid/section-container >}}

{{< grid/section-container id="events" class="featured-section-row featured-section-row-light-bg">}}
	{{< past_events title="Past Events">}}
{{</ grid/section-container >}}
