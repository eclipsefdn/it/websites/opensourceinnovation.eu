---
title: "Open Research Webinars"
tagline: "Open collaboration in European Research Projects"
event: "The Open Research Webinars are over. <br> Subscribe to these mailing lists to stay informed about upcoming events."
date: 2020-12-15T16:00:00+02:00
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
layout: single
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
container: container-fluid
links: [[href: "http://mail.ow2.org/wws/subscribe/opensourcewebinars", text: OW2 Webinars mailing list], [href: "http://eepurl.com/hVoygX", text: Eclipse Research mailing list]]
links2: [[href: "#projects", text: Watch previous episodes]] 
---


{{< grid/section-container id="about" class="featured-section-row featured-section-row-lighter-bg" >}}
{{< grid/section-container class="featured-section-row text-center">}}
<h1>About Open Research Webinars</h1>
<p>
Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.
</p>
<h2 id="next">The Open Research Webinars are over</h2>
<p>
We thank everybody who participated and contributed to this experience. All episodes have been recorded and are ready to be watched. Just click on a project or episode below. 
</p>
<!--
<a href="https://eclipse.org/steady" target="_blank"><img src="2022/january/images/steady.png" width="200" alt="Steady"></a>
<a href="https://www.sat4j.org/" target="_blank"><img src="2022/january/images/sat4j.png" width="200" alt="Sat4j"></a>
-->
{{</ grid/section-container >}}
{{</ grid/section-container >}}


{{< grid/section-container id="projects" class="featured-section-row featured-section-row-dark-bg">}} 
	{{< past_projects title="Presented Projects">}}
{{</ grid/section-container >}}

{{< grid/section-container id="events" class="featured-section-row featured-section-row-light-bg">}}
	{{< past_events title="Past Events">}}
{{</ grid/section-container >}}
