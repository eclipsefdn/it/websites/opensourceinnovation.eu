---
title: "Open Research Webinars - March 4, 2021"
tagline: "Open collaboration in European research projects"
event: "March 4, 2021 - 16:00-17:00 CET"
completed: true
date: 2021-03-04T16:00:00+02:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
hide_breadcrumb: true
container: container-fluid
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
links: [[href: "#speakers", text: Speakers], [href: "#agenda", text: Agenda]]
layout: single
---
{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg" >}}
  {{< events/registration event="march" year="2021" title="About the Event" >}} 

Join us for our next Open Research Webinars on Thursday, March 4, 2021 at 16:00 CET. 

Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.

### Presentations
[![DECODER](images/decoder.png)](https://www.decoder-project.eu) [![PDP4E](images/pdp4e.png)](https://www.pdp4e-project.eu/)

  {{</ events/registration >}}
{{</ grid/section-container >}}

{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="march" year="2021" title="Speakers" source="speakers" imageRoot="/2021/march/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="march" year="2021" >}}
{{</ grid/section-container >}}

{{< grid/section-container id="projects" class="featured-section-row featured-section-row-dark-bg">}} 
	{{< past_projects title="Presented Projects">}}
{{</ grid/section-container >}}

{{< grid/section-container id="events" class="featured-section-row featured-section-row-light-bg">}}
	{{< past_events title="Past Events">}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}