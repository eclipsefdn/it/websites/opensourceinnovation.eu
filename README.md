# opensourceinnovation.eu

The [opensourceinnovation.eu](https://opensourceinnovation.eu) website is generated with [Hugo](https://gohugo.io/documentation/).

[![Netlify Status](https://api.netlify.com/api/v1/badges/8d42015f-09c7-46b1-9f9c-419404d01f6d/deploy-status)](https://app.netlify.com/sites/eclipsefdn/deploys)

## Getting started

Install dependencies, build assets and start a webserver:

```bash
yarn
hugo server
```

## Request an Event Website

Eclipse Foundation Staff can request an Event website similar to [December](https://opensourceinnovation.eu/2020/december/) by creating an [issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/opensourceinnovation.eu/-/issues/new?issuable_template=event).

The Eclipse Foundation Webdev team will start working on your request once the [Event Website Content Template](https://docs.google.com/document/d/1oVLBK8tzyuYC9OUisy1x-cc50PfGx0alnUHw9RYlZag) is completed and uploaded to your issue.

Please plan for a minimum of two weeks for the website implementation. Additional information is available in the [Event Website Content Template](https://docs.google.com/document/d/1oVLBK8tzyuYC9OUisy1x-cc50PfGx0alnUHw9RYlZag) document.

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [opensourceinnovation.eu](https://gitlab.eclipse.org/eclipsefdn/it/websites/opensourceinnovation.eu) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/opensourceinnovation.eu.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Updating site after event
1. Change index file and use 'index.md.template' as basis
2. In event content folder
	1. add presentations
	2. copy event index file from root to here
		1. set completed: true
		2. replace registratrion link with links to speakers and agenda
3. In data folder
	1. Add projects in conferences.yml
	2. In agenda.yml
		1. set complete: true
		2. add links to slides and videos
		3. comment out time
4. Add project logoes in layouts/shortcodes/past-projects.html

## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel. 

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/opensourceinnovation.eu/-/issues).


## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [opensourceinnovation.eu authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/opensourceinnovation.eu/-/graphs/master). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/opensourceinnovation.eu/-/raw/master/README.md).
